FIRMWARE_IMAGES := \
    bootloader \
    keystorage \
    ldfw \
    logo \
    modem

AB_OTA_PARTITIONS += $(FIRMWARE_IMAGES)
