FIRMWARE_IMAGES := \
    abl \
    bluetooth \
    cmnlib \
    cmnlib64 \
    devcfg \
    dsp \
    hyp \
    keymaster \
    mdtp \
    mdtpsecapp \
    modem \
    pmic \
    rpm \
    tz \
    xbl

AB_OTA_PARTITIONS += $(FIRMWARE_IMAGES)
