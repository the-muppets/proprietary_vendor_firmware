LOCAL_PATH := $(call my-dir)

define target-radio-files
$(notdir \
  $(wildcard $(LOCAL_PATH)/filesmap) \
  $(wildcard $(LOCAL_PATH)/*.bin) \
  $(wildcard $(LOCAL_PATH)/*.elf) \
  $(wildcard $(LOCAL_PATH)/*.img) \
  $(wildcard $(LOCAL_PATH)/*.mbn) \
)
endef

ifneq ($(wildcard vendor/firmware/$(TARGET_DEVICE)/firmware.mk),)
include vendor/firmware/$(TARGET_DEVICE)/firmware.mk
else ifneq ($(wildcard vendor/firmware/$(TARGET_REFERENCE_DEVICE)/firmware.mk),)
include vendor/firmware/$(TARGET_REFERENCE_DEVICE)/firmware.mk
endif
